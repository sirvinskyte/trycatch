﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Skaiciuok_Click(object sender, EventArgs e)
        {
            double x=0;
            double y=0;
            try
            {
                x = Convert.ToDouble(textBox1.Text);
            }
            catch
            {
                MessageBox.Show("blogai ivestas skaicius a");
                return;
            }
            try
            {
                y = Convert.ToDouble(textBox2.Text);
            }
            catch
            {
                MessageBox.Show("blogai ivestas skaicius b");
                return;
            }

            textBox3.Text = (x+y).ToString(); 

        }
    }
}
